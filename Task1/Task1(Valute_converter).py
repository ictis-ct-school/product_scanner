class Valute_converter:
    def __init__(self, summ):
        self.summ = summ
    nominals = [5000, 2000, 1000, 500, 200, 100, 50, 10, 5, 2, 1]
    count_nominals = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    def _to_dollar(self):
        return 61.77*self.summ
    
    def _to_euro(self):
        return 64.99*self.summ
    
    def _to_rupia(self):
        return 0.758*self.summ
    
    def _to_vona(self):
        return 0.04752*self.summ
    
    def _to_peso(self):
        return 3.18*self.summ
    
    def _parse(self): 
        tmp = int(self.summ)
        while tmp > 0:
            for i in range(11):
                if tmp-self.nominals[i]>=0:
                    tmp-=self.nominals[i]
                    self.count_nominals[i]+=1
                    break
        return list(self.nominals)
        

print("Это конвертер валют"+"\N{winking face}"+"\nДля того, чтобы выполнить перевод денег с рублей в другую валюту, пользуйтесь конструкцией: convert ru {обозначение валюты} {величина в рублях}\nПеречень всех доступных валют с обозначениями:\ndo(Доллары), eu(Евро), rp(Рупия), vn(Вона), ps(Песо)\nТакже возможно распределить введенную сумму рублей по купюрам/монетам по приоритету величины номинала. Конструкция: parse {сумма в рублях}\nВыходные данные сохраняются в файле results.txt")
file = open("results.txt", "w+")
# valutes = ["dollar", "euro", "rupia", "vona", "peso"]

while True:
    try:
        full_phrase = str(input())
        
        
        if full_phrase[0:5] == "parse":
            val = int(full_phrase[6:])
            c = Valute_converter(val)
            c._parse()
            file.write(str(c.count_nominals)+"\n")
        else:
            
            if full_phrase[0:7] == "convert":
                val = int(full_phrase[14:])
                valute = full_phrase[11:13]
                c = Valute_converter(val)
                if valute == "eu":
                    file.write(str(c._to_euro())+"\n")
                else:
                    if valute == "rp":
                        file.write(str(c._to_rupia())+"\n") 
                    else:
                        if valute == "do":
                            file.write(str(c._to_dollar())+"\n")
                            
                        else:
                            if valute == "vn":
                                file.write(str(c._to_vona())+"\n")
                                
                            else:
                                if valute == "ps":
                                    file.write(str(c._to_peso())+"\n")
                                    
            else:
                
                file.close()
                quit(0)
        
    except:
        print("Программа завершена")
        file.close()
        quit(0)
    





    
