import numpy as np
import matplotlib.pyplot as plt
import keras
from keras import Sequential
from keras.layers import Dense
from tensorflow import keras

c=np.array([
    [1,1,1,1],
   [1,1,1,0],
   [1,1,0,1],
   [1,0,1,1],
   [0,1,1,1],
   [0,0,0,0],
   [1,0,0,0],
   [0,1,1,0],
   [1,1,0,0],
   [1,0,1,0]
   ])
f=np.array([1, 1, 0.6, 0.6, 0, 0, 0, 0, 1, 0])
#ii=>0.93 - готовить дома, 0.1 < ii < 0.93 -  заказывать, ii < 0.1 - не поешь
model = keras.Sequential()
model.add(Dense(units=1, activation='sigmoid'))

model.compile(loss='mean_squared_error', optimizer=keras.optimizers.Adam(0.1))

history = model.fit(c, f, epochs=600, verbose=0)

plt.plot(history.history["loss"])
plt.grid(True)
plt.show()

def inputData():
  print("Ввод Да или Нет")
  a=str(input("Есть ли деньги?  "))
  if a == "да" or a == "Да" or a == "ДА" or a == "дА" or a == "Да " or a == "ДА " or a == "да ":
    a = 1
  else:
    a = 0
  b=str(input("Есть ли время на готовку?  "))
  if b == "да" or b == "Да" or b == "ДА" or b == "дА" or b == "Да " or b == "ДА " or b == "да ":
    b = 1
  else:
    b = 0
  c=str(input("Есть ли желание приготовить самостоятельно?  "))
  if c == "да" or c == "Да" or c == "ДА" or c == "дА" or c == "Да " or c == "ДА " or c == "да ":
    c = 1
  else:
    c = 0
  d=str(input("Есть ли в онлайн-магазине нужное нам блюдо?  "))
  if d == "да" or d == "Да" or d == "ДА" or d == "дА" or d == "Да " or d == "ДА " or d == "да ":
    d = 1
  else:
    d = 0
  
  array=[a,b,c,d]
  return array

def otv(array):
  ii=array[0][0]
  print(ii)
  if ii>=0.9:
    return "\n\nГотовь дома"
  elif ii<0.9 and ii >= 0.2:
    return "\n\nЗаказывай в интернет-магазине"
  else:
    return "\n\nПридется поесть в следующий раз"

    
reshenie=model.predict([inputData()])
print(otv(reshenie))
